<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;

use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $users = User::orderBy('id', 'DESC')->paginate(30);

            $response = [
                'status' => 200,
                'message' => "Usuarios cargados exitosamente.",
                'users' => $users
            ];

            return $response;
        } catch (\Exception $e) {

            dd($e);

            $response = [
                'status' => 500,
                'message' => "Lo sentimos ha ocurrido un error, intentalo mas tarde.",
            ];

            return $response;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {

            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;

            if ($request->filled('password')) {
                $user->password = $request->password;
            } else {
                $user->password = Str::random(8);
            }

            if ($user->save()) {

                broadcast(new UserWasCreated($user));

                $response = [
                    'status' => 200,
                    'message' => "Usuario creado exitosamente",
                    'user' => $user
                ];
                
            } else {

                $response = [
                    'status' => 404,
                    'message' => "Ups, lo sentimos, ha ocurrido un error durante la creación",
                ];
            }

            return $response;
            
        // } catch (\Exception $ex) {

            // dd($ex);

            // $response = [
            //     'status' => 500,
            //     'message' => "Lo sentimos ha ocurrido un error, intentalo mas tarde.",
            // ];

            // return $response;
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        try {

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;

        } catch (\Exception $ex) {

            dd($ex);

            $response = [
                'status' => 500,
                'message' => "Lo sentimos ha ocurrido un error, intentalo mas tarde.",
            ];

            return $response;

        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
